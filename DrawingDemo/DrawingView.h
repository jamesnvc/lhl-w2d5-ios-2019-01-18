//
//  DrawingView.h
//  DrawingDemo
//
//  Created by James Cash on 18-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DrawingView : UIView

@end

NS_ASSUME_NONNULL_END
