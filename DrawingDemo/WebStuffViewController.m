//
//  WebStuffViewController.m
//  DrawingDemo
//
//  Created by James Cash on 18-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "WebStuffViewController.h"
@import WebKit;
@import SafariServices;

@interface WebStuffViewController () <WKNavigationDelegate>
@property (weak, nonatomic) IBOutlet UITextField *urlTextField;

@end

@implementation WebStuffViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (IBAction)websiteInVC:(id)sender {
    NSURL *theURL = [NSURL URLWithString:self.urlTextField.text];
    SFSafariViewController* svc = [[SFSafariViewController alloc] initWithURL:theURL];
    [self presentViewController:svc animated:YES completion:^{
        NSLog(@"Shown in safari");
    }];
}

- (IBAction)goToWebsite:(id)sender {
    [self.urlTextField resignFirstResponder];
    WKWebView *webView = [[WKWebView alloc] init];
    webView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:webView];

    [NSLayoutConstraint activateConstraints:
     @[[webView.bottomAnchor constraintEqualToAnchor:self.view.bottomAnchor],
       [webView.centerXAnchor constraintEqualToAnchor:self.view.centerXAnchor],
       [webView.topAnchor constraintEqualToAnchor:self.view.centerYAnchor],
       [webView.widthAnchor constraintEqualToAnchor:self.view.widthAnchor]
       ]];

    NSURL *theURL = [NSURL URLWithString:self.urlTextField.text];
    [webView loadRequest:[NSURLRequest requestWithURL:theURL]];
    webView.navigationDelegate = self;
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation
{
    [webView evaluateJavaScript:@"document.body.style.backgroundColor = \"red\";" completionHandler:^(id _Nullable res, NSError * _Nullable error) {
        NSLog(@"Ran javascript %@", res);
    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
