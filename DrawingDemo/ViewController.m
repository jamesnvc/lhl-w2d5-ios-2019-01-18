//
//  ViewController.m
//  DrawingDemo
//
//  Created by James Cash on 18-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "CustomView.h"

@interface ViewController () <UITextFieldDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    CustomView* v = [[CustomView alloc] initWithFrame:CGRectMake(20, 20, 200, 350)];
    v.defaultRingColour = [UIColor purpleColor];
    v.pressedRingColour = [UIColor orangeColor];
    [self.view addSubview:v];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
//    textField.text;
    return YES;
}

@end
