//
//  DrawingView.m
//  DrawingDemo
//
//  Created by James Cash on 18-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "DrawingView.h"

@interface DrawingView ()
@property (nonatomic,strong) NSMutableArray<UIBezierPath*>* lines;
@end

@implementation DrawingView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    for (UIBezierPath* path in self.lines) {
        [[UIColor redColor] setStroke];
        [path stroke];
    }
    // other way would be like
    /*
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetLineWidth(ctx, 5);
    for (NSArray<NSValue*>* line in self.lines) {
        CGPoint firstPoint = line.firstObject.CGPointValue;
        CGContextMoveToPoint(ctx, firstPoint.x, firstPoint.y);
        for (NSValue* pointV in line) {
            CGPoint p = pointV.CGPointValue;
            CGContextAddLineToPoint(ctx, p.x, p.y);
        }
    }
    */
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if (!self.lines) {
        self.lines = [[NSMutableArray alloc] init];
    }
    UIBezierPath* newLine = [UIBezierPath bezierPath];
    [newLine setLineWidth:5];
    [newLine moveToPoint:[touches.anyObject locationInView:self]];
    [self.lines addObject:newLine];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [[self.lines lastObject] addLineToPoint:[touches.anyObject locationInView:self]];
    [self setNeedsDisplay];
}

@end
