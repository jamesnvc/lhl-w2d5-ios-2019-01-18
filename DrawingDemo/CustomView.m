//
//  CustomView.m
//  DrawingDemo
//
//  Created by James Cash on 18-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import "CustomView.h"

@interface CustomView ()
@property (nonatomic,strong) UIColor *ringColour;

@end

@implementation CustomView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    if (!self.ringColour) {
        self.ringColour = self.defaultRingColour ? self.defaultRingColour : [UIColor yellowColor];
    }
    CGContextRef ctx = UIGraphicsGetCurrentContext();

    CGContextSetFillColorWithColor(ctx, [UIColor blueColor].CGColor);
    CGContextFillRect(ctx, self.bounds);

    CGContextSetStrokeColorWithColor(ctx, self.ringColour.CGColor);
    CGContextSetLineWidth(ctx, 10);
    CGContextStrokeEllipseInRect(ctx, CGRectInset(self.bounds, 20, 20));
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    self.ringColour = self.pressedRingColour ? self.pressedRingColour : [UIColor redColor];
    [self setNeedsDisplay];
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    self.ringColour = self.defaultRingColour ? self.defaultRingColour : [UIColor yellowColor];
    [self setNeedsDisplay];
}


@end
