//
//  CustomView.h
//  DrawingDemo
//
//  Created by James Cash on 18-01-19.
//  Copyright © 2019 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

// IB_DESIGNABLE makes it so that our custom view shows up in storyboard with the custom draw method we gave it (instead of just looking like a plain view)
IB_DESIGNABLE
@interface CustomView : UIView

@property (nonatomic,strong) IBInspectable UIColor *defaultRingColour;
@property (nonatomic,strong) IBInspectable UIColor *pressedRingColour;

@end

NS_ASSUME_NONNULL_END
